---
geometry: a5paper,margin=1cm
header-includes: \usepackage[fontsize=8.75pt]{fontsize}
---

\begin{center}
\LARGE{Linux auf Smartphones}\\[0.5cm]
\large{Online-Version: \url{https://pinephone.de/flyer.pdf}}\\[0.25cm]
\large{Repository: \url{https://framagit.org/loss/flyer} | Lizenz: CC BY-SA 4.0}\\[0.5cm]
\end{center}

\pagenumbering{gobble}

Du freust dich, denn deine Server, Desktops und Laptops laufen mit Linux.
Ist es nicht fantastisch, zu einem großen Teil frei von properitärer
Software zu sein, und diese Systeme komplett unter deiner Kontrolle zu haben?
Eine typische Linux-Distribution müllt dich nicht mit Werbung zu, respektiert
deine Privatsphäre und liefert dir Updates, bis dein Gerät physisch nicht
mehr nutzbar ist. Falls du dich etwas intensiver mit Linux beschäftigt hast,
kannst du wahrscheinlich auch über ein Terminal und SSH einfach alles
konfigurieren und automatisieren.

Doch was ist mit deinem Smartphone? Wäre es nicht schön, wenn du statt iOS oder
Android dort auch eine richtige Linux-Distribution verwenden könntest? Es gibt
ein ganzes Ökosystem von Projekten, die dieses Ziel verfolgen.

Wie auf dem Desktop gibt es verschiedene Oberflächen, von den mobilen Varianten
für KDE und GNOME bis hin zu einem Tiling Window Manager, den man mit
Wisch-Gesten bedienen kann. Es gibt Distributionen, die diese Oberflächen
paketieren und für eine Vielzahl von Smartphones zur Verfügung stellen. Und es
gibt Gesamtsysteme aus Oberflächen und Distributionen. Nicht alle Projekte sind
in einem alltagstauglichen Zustand, aber dennoch interessant für Linux-Enthusiasten,
Bastler und (angehende) Entwickler, die selbst etwas am Status Quo
ändern wollen.

## Geräte

-   PINE64 (<https://pine64.org>): PinePhone und PinePhone Pro
-   Purism (<https://puri.sm>): Librem 5
-   viele Android-Geräte (teilweise mit Mainline Kernel), unterstützt von
    -   postmarketOS (<https://wiki.postmarketos.org/wiki/Devices>)
    -   Ubuntu Touch (<https://devices.ubuntu-touch.io>)
    -   Mobian (<https://wiki.mobian.org/doku.php?id=devices>)
    -   Droidian (<https://devices.droidian.org/devices/>)
    <!--TODO-->
-   umfangreiche Liste auf TuxPhones.com (<https://many.tuxphones.com>)

## Oberflächen / Ökosysteme

-   Plasma Mobile (<https://plasma-mobile.org>)
-   Phosh (<https://puri.sm/pureos/phosh/>)
-   Sxmo (<https://sxmo.org>)
<!-- TODO: Besserer Link -->
-   GNOME Shell on Mobile (<https://blogs.gnome.org/shell-dev/2022/09/09/gnome-shell-on-mobile-an-update/>)

## Distributionen

-   basierend auf Arch Linux
    -   Manjaro (<https://github.com/manjaro-pinephone>)
    -   Arch Linux ARM on Mobile (<https://github.com/dreemurrs-embedded/Pine64-Arch>)
    -   Kupfer (<https://kupfer.gitlab.io>)
-   basierend auf Debian
    -   PureOS (<https://pureos.net>)
    -   Mobian (<https://mobian-project.org>)
    -   Droidian (<https://droidian.org>)
-   postmarketOS (<https://postmarketos.org>)
-   Maemo Leste (<https://maemo-leste.github.io>)
-   Mobile NixOS (<https://mobile.nixos.org>)
-   Rhino Linux (<https://rhinolinux.org>)
-   Liste mit weiteren auf LINMOB.net (<https://linmob.net/resources/#mobile-linux-distributions>)
<!-- * AVMultiPhone -->

## Gesamtsysteme

-   Ubuntu Touch (<https://ubports.com>)
-   SailfishOS (<https://sailfishos.org>)

## Apps

-   App-Listen
    -   LinuxPhoneApps (<https://linuxphoneapps.org>)
    -   Liste im Mobian-Wiki (<https://wiki.mobian-project.org/doku.php?id=apps>)
    -   Listen für PureOS
        -   <https://tracker.pureos.net/w/pureos/mobile_optimized_apps/>
        -   <https://tracker.pureos.net/w/pureos/3rd-party_mobile_optimized_apps/>
-   Android-Apps auf Linux-Smarphones
    -   Waydroid (<https://waydro.id>)

## Blogs

-   fossphones (<https://fossphones.com/news.html>)
-   LINMOB.net (<https://linmob.net>)
-   TuxPhones (<https://tuxphones.com>)

## Podcasts

-   (de) Linux Smartphones (Episode von FOCUS ON: Linux) (<https://focusonlinux.podigee.io/50-linux-smartphones>)
-   PineTalk (<https://www.pine64.org/pinetalk/>)
-   postmarketOS podcast (<https://cast.postmarketos.org>)

## Chats

-   (de) Rund ums PinePhone (Matrix Chatraum)
    (<https://matrix.to/#/#PP_german:matrix.org>)
-   FOSS Mobile (Matrix Space mit Chaträumen zu den meisten Projekten)
    (<https://matrix.to/#/#FOSSMobile:matrix.org>)

<!--## Kurioses
* Doom auf PinePhone-Modem:
* beleuchtete PinePhone-Tastatur: -->

<!-- ## Bootloader / Low Level-Zeug / Firmware -->
